import main
import methode_dechiffrage
import methode_chiffrage

def dechiffrage_choix():
    if(main.etat_change.get() == True):
        main.label_mode["text"]="Dechiffrage circulaire"
        methode_dechiffrage.dechiffrage_circulaire(main.phrase.get())
    else:
        main.label_mode["text"]="Dechiffrage non circulaire"
        methode_dechiffrage.dechiffrage_non_circulaire(main.phrase.get())

def chiffrage_choix():
    if(main.etat_change.get() == True):
        main.label_mode["text"]="Chiffrage circulaire"
        methode_chiffrage.chiffrage_circulaire(main.phrase.get())
    else:
        main.label_mode["text"]="Chiffrage non circulaire"
        methode_chiffrage.chiffrage_non_circulaire(main.phrase.get())
    
