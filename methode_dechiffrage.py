import main
import methode_affichage

def dechiffrage_non_circulaire(phrase):                         ##fonction permettant de decrypter une chaine de caractÃ¨re en modifiant les caractÃ¨re avec un decalage defini
    phrase_dechiffree = []                            ##definition d'une liste vide
    for caractere in phrase:                         ##decoupage des caractere de la chaine de caratÃ¨re
        valeur = ord(caractere)                      ##recuperation de la valeur ascii du caractÃ¨re
        valeur = valeur - main.valeur_decalage.get()                        ##modification de valeur ascii
        phrase_dechiffree.append(chr(valeur))         ##ajoute le caractÃ¨re modifie a la fin de la liste de caracteere
    phrase_dechiffree = "".join(phrase_dechiffree)     ##Transforme la liste de caractÃ¨re en chaine de caractere
    methode_affichage.afficher(phrase_dechiffree)

def dechiffrage_circulaire(phrase):
    phrase_dechiffree = []                                       ##definition d'une liste vide
    liste_chiffree = main.liste_Alphabet[main.valeur_decalage.get():]+main.liste_Alphabet[:main.valeur_decalage.get()]   ##Permet de crypter uniquement les lettre de l'alphabet et non les symbole, chiffre a l'aide de la cle passe en parametre
    liste_dechiffree = liste_chiffree[:main.valeur_decalage.get()]+liste_chiffree[main.valeur_decalage.get():] ##Permet de decrypter la chaine crypter a l'aide de la cle passe en parametre
    for caractere in phrase:                                    ##decoupage des caractÃ¨re de la chaine de caratere
        try:
            index = liste_dechiffree.index(caractere)      ##Recupere l'index du caractÃ¨re dans la chaine non crypter(compose uniquement de lettre)
            phrase_dechiffree.append(main.liste_Alphabet[index])         ##Ajoute a la liste de caractere la valeur, situe a l'index recupere precedement, dans la chaine crypter 
        except ValueError:                                      ## si la valeur n'est pas prÃ©sente
             phrase_dechiffree.append(caractere)
    phrase_dechiffree = "".join(phrase_dechiffree)                ##la copie sans transformation
    methode_affichage.afficher(phrase_dechiffree)



