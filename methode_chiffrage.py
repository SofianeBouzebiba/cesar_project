import main
import methode_affichage

def chiffrage_non_circulaire(phrase):                          ##fonction permettant de crypter une chaine de caractÃ¨re en modifiant les caractÃ¨re avec un dÃ©calage defini
    phrase_chiffree = []                             ##definition d'une liste vide
    for caractere in phrase:                        ##decoupage des caractÃ¨re de la chaine de caratere
        valeur = ord(caractere)                     ##recuperation de la valeur ascii du caractÃ¨re
        valeur = valeur + main.valeur_decalage.get()                       ##modification de valeur ascii
        phrase_chiffree.append(chr(valeur))          ##ajoute le caractere modifie a la fin de la liste de caractÃ¨re
    phrase_chiffree = "".join(phrase_chiffree)        ##Transforme la liste de caractÃ¨re en chaine de caractÃ¨re
    methode_affichage.afficher(phrase_chiffree)


def chiffrage_circulaire(phrase):
    main.label_mode["text"]="Chiffrage"
    phrase_chiffree = []                                         ##definition d'une liste vide
    liste_chiffree = main.liste_Alphabet[main.valeur_decalage.get():]+main.liste_Alphabet[:main.valeur_decalage.get()]   ##Permet de crypter uniquement les lettre de l'alphabet et non les symbole, chiffre a l'aide de la cle passe en parametre
    for caractere in phrase:                                    ##decoupage des caractÃ¨re de la chaine de caratere
        try:
            index = main.liste_Alphabet.index(caractere)                ##Recupere l'index du caractere dans la chaine non crypter(compose uniquement de lettre)
            phrase_chiffree.append(liste_chiffree[index])   ##Ajoute a la liste de caractÃ¨re la valeur, situe a l'index recupere precedement, dans la chaine crypter 
        except ValueError:                                      ## si la valeur n'est pas prÃ©sente
             phrase_chiffree.append(caractere)                   ##la copie sans transformation
    phrase_chiffree = "".join(phrase_chiffree)                    ##Transforme la liste de caractÃ¨re en chaine de caractÃ¨re
    methode_affichage.afficher(phrase_chiffree)



