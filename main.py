import string
from tkinter.messagebox import *
from tkinter import * 

########################################################################
#                                NEEDS
########################################################################
# ''' VERIFICATION VALIDITE TEXTE ''''
# '''' VERIFICATION VALIDITE VALEUR   ''''
# COMMENTAIRES DESCRIPTIFS
# PROBLEME IMPORT : DEPLACEMENT DES METHODES DANS D'AUTRES FICHIERS 
########################################################################
# authors : BOUZEBIBA SOFIANE 
#           DECOUTTERE ALEX
# group : 4
########################################################################


#################### 2) METHODE DECHIFFRAGE ########################

#mÃ©thode permettant de dechiffrer une chaine de caractere de facon non circulaire
def dechiffrage_non_circulaire(phrase):                         
    phrase_dechiffree = []                            #creation d'une liste phrase_decrypter comportera le resultat de la methode
    for caractere in phrase:                         #parcours de chaque caractere de la phrase
        valeur = ord(caractere)                      #valeur prend la valeur ascii du caractÃ¨re
        valeur = valeur - valeur_decalage.get()                        #valeur - decalage
        phrase_dechiffree.append(chr(valeur))         #ajoute le caractÃ¨re modifie a la fin de la liste de caractÃ¨re
    phrase_dechiffree = "".join(phrase_dechiffree)     #Transforme la liste de caractÃ¨re en chaine de caractÃ¨re
    afficher(phrase_dechiffree)                         #fait appel a la fonction affiche pour afficher le resultat

#mÃ©thode permettant de dechiffrer une chaine de caractere de facon circulaire
def dechiffrage_circulaire(phrase):
    phrase_dechiffree = []                                       ##definition d'une liste vide
    liste_chiffree = liste_Alphabet[valeur_decalage.get():]+liste_Alphabet[:valeur_decalage.get()]   ##Permet de crypter uniquement les lettre de l'alphabet et non les symbole, chiffre a l'aide de la cle passe en parametre
    liste_dechiffree = liste_chiffree[:valeur_decalage.get()]+liste_chiffree[valeur_decalage.get():] ##Permet de decrypter la chaine crypter a l'aide de la cle passe en parametre
    for caractere in phrase:                                    ##dÃ©coupage des caractÃ¨re de la chaine de caratÃ¨re
        try:
            index = liste_dechiffree.index(caractere)      ##Recupere l'index du caractÃ¨re dans la chaine non crypter(compose uniquement de lettre)
            phrase_dechiffree.append(liste_Alphabet[index])         ##Ajoute a la liste de caractÃ¨re la valeur, situe a l'index recupere precedement, dans la chaine crypter 
        except ValueError:                                      ## si la valeur n'est pas prÃ©sente
             phrase_dechiffree.append(caractere)
    phrase_dechiffree = "".join(phrase_dechiffree)                ##la copie sans transformation
    afficher(phrase_dechiffree)

#################### 2) METHODE CHIFFRAGE #########################

#mÃ©thode permettant de dechiffrer une chaine de caractere de facon non circulaire
def chiffrage_non_circulaire(phrase):                          ##fonction permettant de crypter une chaine de caractÃ¨re en modifiant les caractÃ¨re avec un dÃ©calage defini
    phrase_chiffree = []                             ##definition d'une liste vide
    for caractere in phrase:                        ##dÃ©coupage des caractÃ¨re de la chaine de caratÃ¨re
        valeur = ord(caractere)                     ##recuperation de la valeur ascii du caractÃ¨re
        valeur = valeur + valeur_decalage.get()                       ##modification de valeur ascii
        phrase_chiffree.append(chr(valeur))          ##ajoute le caractÃ¨re modifie a la fin de la liste de caractÃ¨re
    phrase_chiffree = "".join(phrase_chiffree)        ##Transforme la liste de caractÃ¨re en chaine de caractÃ¨re
    afficher(phrase_chiffree)

#mÃ©thode permettant de chiffrer une chaine de caractere de facon circulaire
def chiffrage_circulaire(phrase):
    phrase_chiffree = []                                         ##definition d'une liste vide
    liste_chiffree = liste_Alphabet[valeur_decalage.get():]+liste_Alphabet[:valeur_decalage.get()]   ##Permet de crypter uniquement les lettre de l'alphabet et non les symbole, chiffre a l'aide de la cle passe en parametre
    for caractere in phrase:                                    ##dÃ©coupage des caractÃ¨re de la chaine de caratÃ¨re
        try:
            index = liste_Alphabet.index(caractere)                ##Recupere l'index du caractÃ¨re dans la chaine non crypter(compose uniquement de lettre)
            phrase_chiffree.append(liste_chiffree[index])   ##Ajoute a la liste de caractÃ¨re la valeur, situe a l'index recupere precedement, dans la chaine crypter 
        except ValueError:                                      ## si la valeur n'est pas prÃ©sente
             phrase_chiffree.append(caractere)                   ##la copie sans transformation
    phrase_chiffree = "".join(phrase_chiffree)                    ##Transforme la liste de caractÃ¨re en chaine de caractÃ¨re
    afficher(phrase_chiffree)


#################### 1) METHODE CHOIX #########################

def dechiffrage_choix():
    if(etat_change.get() == True): #si le radio bouton circulaire est cochÃ© alors 
        label_mode["text"]="Dechiffrage circulaire" #modifie le texte de label_mode en "DÃ©chiffrage circulaire"
        dechiffrage_circulaire(phrase.get()) #fait appel a la fonction dechiffrage_circulaire ayant pour parametre la phrase
    else: 
        label_mode["text"]="Dechiffrage non circulaire" #modifie le texte de label_mode en "DÃ©chiffrage non circulaire"
        dechiffrage_non_circulaire(phrase.get()) #fait appel a la fonction dechiffrage_non_circulaire ayant pour parametre la phrase

def chiffrage_choix():
    if(etat_change.get() == True): #si le radio bouton circulaire est cochÃ© alors 
        label_mode["text"]="Chiffrage circulaire" #modifie le texte de label_mode en "Chiffrage circulaire"
        chiffrage_circulaire(phrase.get()) #fait appel a la fonction chiffrage_circulaire ayant pour parametre la phrase
    else:
        label_mode["text"]="Chiffrage non circulaire" #modifie le texte de label_mode en "Chiffrage non circulaire"
        chiffrage_non_circulaire(phrase.get()) #fait appel a la fonction chiffrage_non_circulaire ayant pour parametre la phrase

#################### 3) METHODE AFFICHAGE #########################

def afficher(phrase_affiche):
    sortie.delete(0.0,END) #supprime le texte dÃ©ja prÃ©sent
    sortie.insert(0.0,phrase_affiche) #ajoute au debut de la zone de texte le resultat
    print(phrase_affiche) #Affiche le rÃ©sultat sur le terminal




############################################# MAIN ##################################################


liste_Alphabet = string.ascii_lowercase + string.ascii_uppercase  #creation d'une variable contenant l'alphabet (miniscule puis majuscule)


#Fenetre
root = Tk() #DÃ©finition de la fenetre ayant le nom "root"
root.title("Chiffre de cÃ©sar - BOUZEBIBA DECOUTTERE - G4") #la fenetre Ã  pour nom "Chiffre de cÃ©sar - BOUZEBIBA DECOUTTERE - G4"
root.geometry('500x300') #la fenetre a de base la taille 500px sur 300px


#TITRE
label_titre = Label(root, text="Chiffre de CÃ©sar",font=("Arial Bold", 24)) #Titre de taille 24 et police "Arial Bold" et Ã  pour valeur "Chiffre de CÃ©sar"
label_titre.grid(column=2, row=0,pady = 20)

#Setup du texte de base Ã  entrer
phrase = StringVar() #dÃ©fini phrase en tant que string
phrase.set('Az') #phrase a de base la valeur 'Az'

#Entree Texte
entree = Entry(textvariable=phrase, width=50,bg='#ffffff') #le texte correspond Ã  la variable phrase
entree.grid(column=2, row=1)

#Setup de la valeur de decalage Ã  0
valeur_decalage= IntVar()       #defini valeur_dÃ©calage en tant qu'entier
valeur_decalage.set(0)          #defini valeur_dÃ©calage est de base Ã  0

#Entree chiffre decalage
entree_decalage = Spinbox(root, textvariable=valeur_decalage,from_=0, to=999, width=3,bg='#ffffff') #entree decalage varie entre 0 et 999
entree_decalage.grid(column=3,row=1,pady = 10, padx = 15)

#Bouton Chiffrage
button_chiffrage = Button(root, text="Chiffrage", command=lambda :chiffrage_choix(),bg='#ffffff') #lorsqu'il est appuyÃ© fait appel Ã  la methode chiffrage_choix()
button_chiffrage.grid(row=2, column=1,pady = 10, padx = 15)

#Label mode
label_mode = Label(root ,text="...") #label ecrit de base "..."
label_mode.grid(column=2, row=2,pady = 20)

#Bouton Dechiffrage
button_dechiffrage = Button(root, text="Dechiffrage", command=lambda :dechiffrage_choix(),bg='#ffffff') #lorsqu'il est appuyÃ© fait appel Ã  la methode dechiffrage_choix()
button_dechiffrage.grid(row=2, column=3)

#Text Sortie
sortie = Text(width='40',height='3',bg='#ffffff')
sortie.grid(column=2, row=4)


#Setup des etat possible d'un radio button
etat_possible = [True, False]               #decris les etat possible d'un radio bouton
etat_change = BooleanVar()                  #defini etat_change en binaire
etat_change.set(etat_possible[1])           #etat_change a 2 parametres possible: True ou False

#Radio Bouton Circulaire ou non
radio_circulaire = Radiobutton(root, variable=etat_change, text='Circulaire', value=True) #est de base cochÃ© 
radio_circulaire.grid(column=2, row=5,pady = 5)
radio_non_circulaire = Radiobutton(root, variable=etat_change, text='Non circulaire', value=False) #est de base dÃ©cochÃ©
radio_non_circulaire.grid(column=2, row=6)


root.mainloop()